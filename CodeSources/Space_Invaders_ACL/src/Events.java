
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class Events implements KeyListener, MouseListener {

	Engine Mother;

	private boolean leftPressed = false;
	private boolean rightPressed = false;
	private boolean upPressed = false;
	private boolean downPressed = false;
	private boolean anim� = true;
	
	public boolean getAnim�(){
		return anim�;
	}

	public boolean getUp() {
		return upPressed;
	}

	public void setAnim�(boolean anim�) {
		this.anim� = anim�;
	}

	public boolean getLeft() {
		return leftPressed;
	}

	public boolean getRight() {
		return rightPressed;
	}

	public boolean getDown() {
		return downPressed;
	}


	public Events(Engine Mother) {
		this.Mother = Mother;
	}

	// m�thode appel�e lors d'un appui clavier
	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		Mother.key(arg0.getKeyCode());
		// System.out.println(arg0.getKeyCode());

		if (arg0.getKeyCode() == KeyEvent.VK_LEFT)
			leftPressed = true;

		if (arg0.getKeyCode() == KeyEvent.VK_RIGHT)
			rightPressed = true;

		if (arg0.getKeyCode() == KeyEvent.VK_UP)
			upPressed = true;

		if (arg0.getKeyCode() == KeyEvent.VK_DOWN)
			downPressed = true;
		
		if (arg0.getKeyCode()== KeyEvent.VK_SPACE) {
			if (anim� == true) {
				System.out.println("anim� devient false");
				anim� = false;

			}

			else {
				System.out.println("anim� devient true");
				anim� = true;
			}

		}

		
		
	}

	

	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		Mother.key(arg0.getKeyCode());
		// System.out.println(arg0.getKeyCode());

		if (arg0.getKeyCode() == KeyEvent.VK_LEFT)
			leftPressed = false;

		if (arg0.getKeyCode() == KeyEvent.VK_RIGHT)
			rightPressed = false;

		if (arg0.getKeyCode() == KeyEvent.VK_UP)
			upPressed = false;

		if (arg0.getKeyCode() == KeyEvent.VK_DOWN)
			downPressed = false;
		
	

	}

	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	
	


	

}
