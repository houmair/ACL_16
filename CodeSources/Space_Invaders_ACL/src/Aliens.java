import java.util.ArrayList;

import javax.swing.JFrame;

public class Aliens extends Entités {

	private boolean rightDirection = false;
	private boolean leftDirection = false;
	private boolean upDirection = false;
	private boolean downDirection = false;
	private int frequenceTirAliens=200;
	
	
	
	public int getWitdh() {
		return witdh;
	}

	public int getHeight() {
		return height;
	}

	
	public Aliens(Engine engine, Point point, Boolean rightDirection, Boolean leftDirection, Boolean upDirection, Boolean downDirection) {
		super(point);
		this.setHealth(1);
		this.setVitesseEntités(50);
		this.witdh=45;
		this.height=41;
		this.setUpDirection(upDirection);
		this.setDownDirection(downDirection);
		this.rightDirection=rightDirection;
		this.leftDirection=leftDirection;

	}
	
	
	
	public void deplacementAliens(int dx,int dy){
		
		this.posX=this.posX+dx;
		this.posY=this.posY+dy;
		this.point.x=this.point.x+dx;
		this.point.y=this.point.y+dy;
		
	}
	
	

	public boolean collision_aliens_vaisseau(Vaisseau v) {

		if (this.getPoint().getX() == v.getX() && this.getPoint().getY() == v.getY()) {
			return true;
		}
		return false;
	}

	public boolean isRightDirection() {
		return rightDirection;
	}

	public void setRightDirection(boolean rightDirection) {
		this.rightDirection = rightDirection;
	}

	public boolean isLeftDirection() {
		return leftDirection;
	}

	public void setLeftDirection(boolean leftDirection) {
		this.leftDirection = leftDirection;
	}

	public boolean isUpDirection() {
		return upDirection;
	}

	public void setUpDirection(boolean upDirection) {
		this.upDirection = upDirection;
	}

	public boolean isDownDirection() {
		return downDirection;
	}

	public void setDownDirection(boolean downDirection) {
		this.downDirection = downDirection;
	}

	public int getFrequenceTirAliens() {
		return frequenceTirAliens;
	}

	public void setFrequenceTirAliens(int frequenceTirAliens) {
		this.frequenceTirAliens = frequenceTirAliens;
	}

	
}
