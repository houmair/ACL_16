
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Font;
import java.awt.RenderingHints.Key;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

public class Engine extends Thread {

	Graph graph;
	JFrame frame;
	private ArrayList<Tir> tab_tirs = new ArrayList<Tir>();
	private ArrayList<TirAliens> tab_tirs_aliens = new ArrayList<>();
	private ArrayList<Aliens> tab_aliens = new ArrayList<Aliens>();
	private int recupBonus = 0;
	private ArrayList<Bonus> tab_bonus = new ArrayList<Bonus>();
	private Point pointInit = new Point(300, 400);
	private Vaisseau v = new Vaisseau(pointInit);
	private Events E = new Events(this);
	private long Interval_tir = 250;
	private long Dernier_tir = 0;
	private int vitesseAliens = 1;
	private int frequenceTirAliens= 200;
	private int aliensTu�s = 0;
	private int nbAliensTu�sPremierNiveau = 2;
	private Button boutonRecommencerOui = new Button("OUI");
	private Button boutonRecommencerNon = new Button("NON");
	private JLabel label = new JLabel(" ");
	private int score = 0;
	private int nbBonus = 0;
	private JLabel scoreJLabel = new JLabel(" ");
	private JPanel sud = new JPanel();
	private Boolean anim�1 = true;
	private Boolean appuiSurOui = true;
	private int niveauSuivant = 0;
	private int timer = 150;

	// Notre constructeur
	Engine() {

		graph = new Graph();
		frame = new JFrame();

		// titre de la fenetre
		frame.setTitle("Space Invaders");
		// dimensions de la fen�tre
		frame.setSize(1600, 1000);
		// positionne la fen�tre au milieu de l'�cran
		frame.setLocationRelativeTo(null);
		// ferme la fen�tre si on appui sur la croix
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// on ajoute addKey(E) � notre fen�tre
		frame.addKeyListener(E);
		frame.setContentPane(graph);

		frame.setVisible(true);
		frame.setResizable(false);
		// on ajoute des actions a nos boutons
		boutonRecommencerOui.addActionListener(new BoutonListenerOUI());
		boutonRecommencerNon.addActionListener(new BoutonListenerNON());
		// on ajoute nos bouton a notre JPanel sud et on rajoute
		// ce JPanel dans le JLabel label
		sud.add(boutonRecommencerOui);
		sud.add(boutonRecommencerNon);
		label.add(sud);
		// initialisation des caracteristiques de label
		Font police = new Font("Tahoma", Font.BOLD, 16);
		label.setFont(police);
		label.setForeground(Color.red);
		label.setHorizontalAlignment(JLabel.CENTER);
		label.setText("");
		scoreJLabel.setFont(police);
		scoreJLabel.setForeground(Color.green);
		frame.add(label, BorderLayout.NORTH);
		frame.add(scoreJLabel, BorderLayout.WEST);

		// on initie les entit�s
		init_entit�s();

		// on commence notre boucle infini
		this.start();

		// on refresh l'affichage
		refresh();

	}

	// initialisation des entit�s/vaisseau/
	public void init_entit�s() {

		// bornes ou afficher nos aliens
		// position max et min pour la cr�ation des aliens
		double minX = 0;
		double maxX = frame.getWidth() - 50;
		double minY = 0;
		double maxY = frame.getHeight() - 50;

		// on cr�er des aliens qui vont dans la direction haute
		for (int i = 0; i < 3; i++) {

			double xAliens = Math.random() * (maxX - minX);
			double yAliens = 0;
			// Aliens(Engine engine, Point point, Boolean rightDirection,
			// Boolean leftDirection, Boolean upDirection, Boolean
			// downDirection)
			tab_aliens.add(new Aliens(this, new Point(xAliens, yAliens), false, false, false, true));

		}

		int xInit = (frame.getWidth() - v.getWitdh()) / 2;
		int yInit = (frame.getHeight() - v.getHeight()) / 2;
		System.out.println("initialisation du vaisseau");

		v.setX(xInit);
		v.setY(yInit);
		System.out.println("v.x ;" + v.getX());

		System.out.println("v.y ;" + v.getY());
		aliensTu�s = 0;

		// anim� = true : on peut rentrer dans la boucle infini
		this.anim�1 = true;

		// calcul du temps du syst�me � cet instant
		this.Dernier_tir = System.currentTimeMillis();

		refresh();
	}

	// notre processus/boucle infinie
	public void run() {
		while (true) {

			// s'active quand appui sur go activ� et barre espace
			while (E.getAnim�() && anim�1) {

				creerAliens();
				creer_tirs_aliens();
				collisionAlienVaisseau();
				collisionBonusVaisseau();
				collisionAliensTirs();
				supprimerTirs();
				deplacementVaisseau();
				appelTicExplosion();
				deplacementAliens();
				deplacementTir();
				afficherScore();
				niveauSuivant();
				finPartie();

				refresh();
				while (appuiSurOui = false) {
					frame.addKeyListener(E);
					appuiSurOui = true;
				}

				// while = false
				// timer
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {

					e.printStackTrace();
				}

				if (E.getAnim�() == false) {

					System.out.println("pause");

				}

			}

			System.out.println("play");

		}

	}

	private void creerAliens() {

		// bornes ou afficher nos aliens
		// position max et min pour la cr�ation des aliens
		double minX = 0;
		double maxX = frame.getWidth() - 50;
		double minY = 0;
		double maxY = frame.getHeight() - 50;
		if (ticAliens()) {
			double xAliens = Math.random() * (maxX - minX);
			double yAliens = 0;
			tab_aliens.add(new Aliens(this, new Point(xAliens, yAliens), false, false, false, true));
			timer = 150;

		}

	}

	public boolean ticAliens() {

		timer--;
		if (timer <= 0)
			return (true);
		return false;

	}

	private void niveauSuivant() {
		// TODO Auto-generated method stub
		if (score >= 300 && niveauSuivant < 1) {
			vitesseAliens++;
			niveauSuivant++;
			// creation bonus
			tab_bonus.add(new Bonus(new Point(0, 0), frame));

		}
		if (score >= 600 && niveauSuivant < 2) {
			vitesseAliens++;
			niveauSuivant++;

		}
		
		if (score >=900 && niveauSuivant <3){
			frequenceTirAliens=frequenceTirAliens-50;
			niveauSuivant++;
		}

		if (score >=1200 && niveauSuivant <4){
			frequenceTirAliens=frequenceTirAliens-50;
			niveauSuivant++;
		}
	}

	private void afficherScore() {
		// TODO Auto-generated method stub
		scoreJLabel.setText("score: " + score);

	}

	// Si le tir est rest� assez longtemps affich� (tic de l'explosion
	// se d�cremente a chaque boucle infini) alors on peut faire
	// disparaitre l'explosion
	public void appelTicExplosion() {
		for (int i = 0; i < tab_tirs.size(); i++) {
			tab_tirs.get(i).ticExplosion();
		}
	}

	// Notre fonction permettant de savoir quand une partie est finie
	// ou non
	public void finPartie() {

		// si les points de vie du vaisseau sont a zeros, fin de partie
		if (v.getHealth() <= 0) {
			System.out.println("la partie est finie !");
			recupBonus = 0;
			score = 0;
			niveauSuivant = 0;
			vitesseAliens = 1;
			v.setHealth(10);
			label.setText("Vous avez perdu !");
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}

			recommencer();

		}

		// Si le tableau des aliens est vide (le vaisseau a tu� tout
		// les aliens) alors la partie est gagn� et on recommence
		if (tab_aliens.isEmpty()) {

			System.out.println("Vous avez gagn� ! ");
			label.setText("Vous avez gagn� !");
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}

			recommencer();

		}
	}

	// notre fonction recommencer qui met anim� � false
	// (on sort de notre 2�me boucle infinie)
	public void recommencer() {

		label.setText("Recommencer?");

		// timer
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}

		this.anim�1 = false;
		frame.add(label, BorderLayout.SOUTH);

	}

	// si appui sur la touche oui quand on demande de recommencer
	// alors anim�= true : on peut rerentrer dans la boucle infini
	class BoutonListenerOUI implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			System.out.println("appui sur oui");
			appuiSurOui = false;
			frame.remove(label);
			for (int i = 0; i < tab_aliens.size(); i++) {
				tab_aliens.remove(i);
			}
			init_entit�s();
			System.out.println(appuiSurOui);
			anim�1 = true;
			// this.start();

		}

	}

	// si appui sur la touche non quand on demande de recommencer
	// alors on quitte le jeu
	class BoutonListenerNON implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			System.out.println("appui sur non");
			System.exit(MAX_PRIORITY);
		}

	}

	// permet de supprimer le tir quand n�cessaire
	public void supprimerTirs() {
		for (int i = 0; i < tab_tirs.size(); i++) {
			// appel le timer tic de la classe Tir (qui renvoit true si le
			// tir peut disaparaitre)
			// if (tab_tirs.get(i).tic())
			// tab_tirs.remove(i);

			// si le tir d�passe le haut de l'�cran
			// on supprime le tir
			if (tab_tirs.get(i).getY() < 0)
				tab_tirs.remove(i);
		}
	}

	// permet de supprimer l'explosion quand n�cessaire
	public void supprimerExplosion() {
		for (int i = 0; i < tab_tirs.size(); i++) {
			// appel le timer ticExplosion de la classe Tir (qui renvoit true si
			// le
			// l'explosion peut disaparaitre)
			if (tab_tirs.get(i).ticExplosion())
				tab_tirs.remove(i);

		}
	}

	// permet de faire d�placer le tir (image png) sur l'�cran
	public void deplacementTir() {
		// verifi simplement que notre tableau de tir n'est pas vide
		if (!tab_tirs.isEmpty()) {
			// on parcourt le tableau de tir
			for (int i = 0; i < tab_tirs.size(); i++) {

				// on fait d�placer le tir (image png) en dy<0
				tab_tirs.get(i).setY(tab_tirs.get(i).getY() - 10);

			}
		}

		if (!tab_tirs_aliens.isEmpty()) {
			// on parcourt le tableau de tir
			for (int i = 0; i < tab_tirs_aliens.size(); i++) {

				// on fait d�placer le tir (image png) en dy>0
				tab_tirs_aliens.get(i).setY(tab_tirs_aliens.get(i).getY() + 10);
			}
		}

	}

	// permet de faire d�placer les aliens de fa�on pr�defini
	public void deplacementAliens() {

		double minX = 0;
		double maxX = frame.getWidth() - 50;
		double minY = 0;
		double maxY = frame.getHeight() - 50;

		for (int i = 0; i < tab_aliens.size(); i++) {

			// Si l'alien va dans la direction droite
			if (tab_aliens.get(i).isRightDirection()) {

				// si on sort du cot� droit, leftDirection devient vrai
				// rightDirection devient faux
				// puis on d�place l'alien vers le bas
				if (tab_aliens.get(i).getX() + 45 > frame.getWidth()) {
					tab_aliens.get(i).setX(0);
					int yAliens = (int) (Math.random() * (maxY - minY));
					tab_aliens.get(i).setY(yAliens);

				} else {
					tab_aliens.get(i).deplacementAliens(vitesseAliens, 0);

				}

			}

			if (tab_aliens.get(i).isLeftDirection()) {

				// si on sort du cot� gauche,
				// puis on d�place l'alien vers la droite
				if (tab_aliens.get(i).getX() < 0) {
					tab_aliens.get(i).setX(frame.getWidth());
					int yAliens = (int) (Math.random() * (maxY - minY));
					tab_aliens.get(i).setY(yAliens);

				} else {
					tab_aliens.get(i).deplacementAliens(-vitesseAliens, 0);

				}

			}

			if (tab_aliens.get(i).isUpDirection()) {

				// si on sort du haut, downdirection
				// up direction devient faux
				// puis on d�place l'alien vers le bas
				if (tab_aliens.get(i).getY() < 0) {
					tab_aliens.get(i).setY(frame.getHeight());
					int xAliens = (int) (Math.random() * (maxX - minX));
					tab_aliens.get(i).setX(xAliens);

				} else {
					tab_aliens.get(i).deplacementAliens(0, -vitesseAliens);

				}

			}

			if (tab_aliens.get(i).isDownDirection()) {

				// si on sort du bas, updirection devient vrai
				// down direction devient faux
				// puis on d�place l'alien vers le haut
				if (tab_aliens.get(i).getY() + 41 > frame.getHeight()) {
					tab_aliens.get(i).setY(0);
					int xAliens = (int) (Math.random() * (maxX - minX));
					tab_aliens.get(i).setX(xAliens);
				} else {
					tab_aliens.get(i).deplacementAliens(0, vitesseAliens);

				}

			}

		}
	}

	// Consruit un tir � partir d'un point (de coordonn�e x,y)
	public void creer_tirs(Point point) {

		// si l'interval entre le tir et le dernier tir n'est pas plus petit
		// que notre interval de tir pr�defini alors la fonction ne fait rien

		if (System.currentTimeMillis() - Dernier_tir < Interval_tir) {
			return;
		}

		// si on a attendu assez longtemps, alors on peut cr�er le tir
		// et on enregistre le temps actuel du tir

		Dernier_tir = System.currentTimeMillis();

		// ajoute un nouveau tir de coordonn�e point(x,y) dans tab_tirs
		tab_tirs.add(new Tir(point.x, point.y));
		if (recupBonus == 1) {
			tab_tirs.add(new Tir(point.x + v.getWitdh(), point.y));
		}

	}

	// Consruit un tir � partir d'un point (de coordonn�e x,y)
	public void creer_tirs_aliens() {

		// si le score est sup�rieur a 1000 alors les aliens peuvent maintenant
		// tirer
		// tous les intervales de tirs
		if (score >= 500) {
			
			if (tab_tirs_aliens.isEmpty()) {
				System.out.println("on initialise le tableau de tir aliens tadam");
				for (int i = 0; i < tab_aliens.size(); i++) {
					tab_tirs_aliens.add(new TirAliens(tab_aliens.get(i).getX(), tab_aliens.get(i).getY()));
				}
			}

			// on parcourt le tableau des aliens
			else {
					for (int j = 0; j < tab_aliens.size(); j++) {
						// alors si le timer de tir est <=0 on ajoute un tir �
						// l'aliens
						if (tab_aliens.get(j).getFrequenceTirAliens() <= 0) {
							tab_aliens.get(j).setFrequenceTirAliens(frequenceTirAliens);
							tab_tirs_aliens.add(new TirAliens(tab_aliens.get(j).getX(), tab_aliens.get(j).getY()));
							
							
						} else {
							tab_aliens.get(j).setFrequenceTirAliens(tab_aliens.get(j).getFrequenceTirAliens() - 1);

						

						

						
						
					}
					

				}
			}
		}
	}

	// regarde si il y a collision entre les aliens et les tirs
	// et agit en fonction
	public void collisionAliensTirs() {
		// On parcourt le tableau des diff�rents tirs
		for (int j = 0; j < tab_aliens.size(); j++) {

			// On parcourt le tableau des diff�rents aliens
			for (int i = 0; i < tab_tirs.size(); i++) {

				if (tab_tirs.get(i).collidesWith(tab_aliens.get(j))) {
					System.out.println(" il y a eu collision!tir 2");
					tab_tirs.remove(i);

					tab_aliens.get(j).setHealth(tab_aliens.get(j).getHealth() - 1);

				}

			}
			
			
			if (tab_aliens.get(j).getHealth() <= 0) {
				tab_aliens.remove(j);
				score += 100;
				aliensTu�s++;
			}
		}
		
		for (int i = 0; i < tab_tirs_aliens.size(); i++) {

			if (tab_tirs_aliens.get(i).collidesWith(v)) {
				System.out.println(" il y a eu collision!tir 2");
				v.setHealth(v.getHealth()-1);

			}

		}
	}

	// d�placement du vaisseau (fl�che gauche, droite, haut bas)
	public void deplacementVaisseau() {

		// fleche gauche
		if (E.getLeft() && 0 < v.getX()) {

			v.setX(v.getX() - 10);

		}

		// fleche haut
		if (E.getUp() == true && 0 < v.getY()) {
			v.setY(v.getY() - 10);
		}

		// fleche droite
		if (E.getRight() && graph.getWidth() > v.getX() + 99) {
			v.setX(v.getX() + 10);

		}

		// fleche bas
		if (E.getDown() && graph.getHeight() > v.getY() + 75) {
			v.setY(v.getY() + 10);

		}

	}

	// collision alien / vaisseau
	public void collisionAlienVaisseau() {
		for (int i = 0; i < tab_aliens.size(); i++) {
			// si il y a collision entre le vaisseau et l'alien, supprime un
			// point de vie au deux
			// et si un des deux � ses points de vie � 0 le d�truit}

			if (v.collidesWith(tab_aliens.get(i))) {

				tab_aliens.get(i).setHealth(tab_aliens.get(i).getHealth() - 1);
				v.setHealth(v.getHealth() - 2);
				System.out.println("point de vie vaisseau :" + v.getHealth());

				if (tab_aliens.get(i).estMort())
					tab_aliens.remove(i);
			}

		}
	}

	public void collisionBonusVaisseau() {
		for (int i = 0; i < tab_bonus.size(); i++) {
			// si il y a collision entre le vaisseau et l'alien, supprime un
			// point de vie au deux
			// et si un des deux � ses points de vie � 0 le d�truit}

			if (v.collidesWithBonus(tab_bonus.get(i))) {
				recupBonus = 1;
				System.out.println("bonus");
				tab_bonus.remove(i);
			}

		}
	}

	// refresh permet de redessiner tout ce qu'il y a redessiner
	// sur l'�cran
	public void refresh() {

		// supprime les elements affich�s sur le graph
		graph.clearEntite();

		// dessine le vaisseau
		graph.paint_vaisseau(v);
		// dessiner bonus
		for (int i = 0; i < tab_bonus.size(); i++) {
			graph.afficher_bonus(tab_bonus.get(i).getPoint());
		}
		// dessine les tirs
		for (int i = 0; i < tab_tirs.size(); i++) {

			graph.afficher_tirs2(tab_tirs.get(i).get_Coord());
		}
		for (int i = 0; i < tab_tirs_aliens.size(); i++) {

			graph.afficher_tirs2(tab_tirs_aliens.get(i).get_Coord());
		}

		// dessine les aliens
		for (int i = 0; i < tab_aliens.size(); i++) {
			graph.afficher_aliens(tab_aliens.get(i).getPoint());
		}

		graph.repaint();
	}

	/**
	 * @param keyCode
	 */
	public void key(int keyCode) {
		// TODO Auto-generated method stub
		// keyCode : gauche : 37; haut : 38; droite : 39; bas : 40; a : 65;

		System.out.println(keyCode);

		// touche a : tir

		if (keyCode == 65 && System.currentTimeMillis() - Dernier_tir > Interval_tir) {
			creer_tirs(new Point(v.getX(), v.getY()));
			System.out.println("un tir a �t� cr�e");

			Dernier_tir = System.currentTimeMillis();

		}

	}

}
